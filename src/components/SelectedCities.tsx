import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { SingleCity } from '../common/types';

interface CityProps {
    city: SingleCity;
    removeCity: (id: number) => void;
};

interface SelectedCitiesProps {
    selectedCities: SingleCity[];
    removeCity: (id: number) => void;
};

const City = ({ city, removeCity }: CityProps) => {
    return (
        <View style={styles.city}>
            <Text style={styles.cityText}>{city.display}</Text>
            <TouchableOpacity onPress={() => removeCity(city.id)}>
                <Text style={styles.closeText}>X</Text>
            </TouchableOpacity>
        </View>
    );
}

const SelectedCities = ({ selectedCities, removeCity }: SelectedCitiesProps) => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Selected cities:</Text>
            <View style={styles.cities}>
                {selectedCities.map((city) => (
                    <City key={city.id} city={city} removeCity={removeCity} />
                ))}
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        padding: 10,
        marginVertical: 10,
        borderWidth: 1,
        margin: 2,
        backgroundColor: '#0077b6',
    },
    title: {
        fontSize: 18,
        color: '#fff',
        fontWeight: 'bold',
        marginLeft: 10,
    },
    cities: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    city: {
        flexDirection: 'row',
        backgroundColor: '#48cae4',
        borderRadius: 20,
        paddingHorizontal: 10,
        paddingVertical: 5,
        margin: 5
    },
    cityText: {
        marginRight: 5,
    },
    closeText: {
        fontWeight: 'bold',
    }
});

export default SelectedCities;