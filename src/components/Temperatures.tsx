import React from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import { SingleCity, TemperaturesProps } from '../common/types';

const Temperatures = ({ temperatures }: TemperaturesProps) => {
    return (
        <ScrollView>
            <View style={styles.temperatures}>
                {temperatures.length > 0 && (<View style={styles.cities} >
                    <Text style={styles.title}>Day</Text>
                    <Text style={styles.cell}> </Text>
                    {
                        [...Array(8)].map((_, index) => (
                            <View key={index}>
                                <Text style={styles.cell}>{index}</Text>
                            </View>
                        ))}
                </View>)}
                {temperatures.map((city) => (
                    <View style={styles.cities} key={city.id}>
                        <Text style={styles.title}>{city.display}</Text>
                        <Text style={styles.cell}>Max / Min</Text>
                        {city.temperatures.map((day) => (
                            <View key={day.id}>
                                <Text style={styles.cell}>{day.max.toFixed()}°C / {day.min.toFixed()}°C</Text>
                            </View>
                        ))}
                    </View>
                ))}
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    temperatures: {
        flexDirection: 'row',
        margin: 2,
        flexWrap: 'wrap',
    },
    cities: {
        borderColor: '#000',
        borderWidth: 1,
        backgroundColor: '#caf0f8',
    },
    title: {
        textAlign: 'center',
        backgroundColor: '#0077b6',
        fontWeight: 'bold',
        color: '#fff',
        paddingHorizontal: 2,
        paddingVertical: 1,
        borderColor: '#000',
        borderBottomWidth: 1,
    },
    cell: {
        textAlign: 'center',
        paddingHorizontal: 2,
        paddingVertical: 1,
    }
});

export default Temperatures;
