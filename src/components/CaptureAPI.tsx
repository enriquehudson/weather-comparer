import React from 'react';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';

interface CaptureAPIProps {
    setApiToken: (apiToken: string) => void;
};

const CaptureAPI: React.FC<CaptureAPIProps> = ({ setApiToken }) => {
    const [apiTokenValue, setApiTokenValue] = React.useState('');

    const handleChange = (e: { nativeEvent: { text: React.SetStateAction<string>; }; }) => {
        setApiTokenValue(e.nativeEvent.text);
    };

    const handleSubmit = () => {
        setApiToken(apiTokenValue);
    };

    return (
        <View style={styles.container}>
            <View style={styles.search}>
                <Text style={styles.title}>Weather comparer</Text>
                <Text style={styles.text}>Please capture the API key</Text>
                <TextInput
                    style={styles.input}
                    placeholder="API Key"
                    onChange={handleChange}
                    value={apiTokenValue}
                />
                <Button title="Save" onPress={handleSubmit} />

            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "center",
        padding: 10,
        marginTop: 20,
    },
    search: {
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    text: {
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    input: {
        width: '80%',
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        padding: 10,
    },
});

export default CaptureAPI;
