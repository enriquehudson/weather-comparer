import React, { useState } from 'react';
import { StyleSheet, Text, View, TextInput, Button, FlatList, TouchableOpacity } from 'react-native';
import useCitiesAPI from '../hooks/useCitiesAPI';
import { SearchProps, SingleCity } from '../common/types';

const Search: React.FC<SearchProps> = ({ addCity }) => {
    const [city, setCity] = useState('');
    const [search, setSearch] = useState('');
    const [suggestionsVisible, setSuggestionsVisible] = useState(false);

    const { cities, isLoading } = useCitiesAPI(search);

    const handleChange = (e: { nativeEvent: { text: React.SetStateAction<string>; }; }) => {
        setCity(e.nativeEvent.text);
    };

    const handleSubmit = () => {
        setSearch(city);
        setSuggestionsVisible(true);
    };

    const handleSelectPlace = (selectedPlace: any) => {
        setCity('');
        setSuggestionsVisible(false);
        addCity(selectedPlace);
    };

    return (

        <View style={styles.container}>
            <View style={styles.search}>
                <TextInput
                    style={styles.input}
                    placeholder="Name of city"
                    onChange={handleChange}
                    value={city}
                />
                <Button title="Search" onPress={handleSubmit} />
            </View>
            {isLoading && <Text>Loading...</Text>}

            {suggestionsVisible && !isLoading && (
                <FlatList
                    data={cities.slice(0, 8)}
                    style={styles.list}
                    keyExtractor={(item: SingleCity) => item.id.toString()}
                    renderItem={({ item }) => (
                        <TouchableOpacity onPress={() => handleSelectPlace(item)}>
                            <Text>{item.display}</Text>
                        </TouchableOpacity>
                    )}
                />
            )}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {

    },
    search: {
        flexDirection: 'row',
        marginLeft: 2,
    },
    input: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        width: 250,
        padding: 4,
    },
    list: {
        width: '90%',
        marginHorizontal: 2,
        backgroundColor: '#caf0f8',
        borderColor: 'gray',
        borderWidth: 1,
        padding: 2,
    }
});

export default Search;