import { useState, useEffect } from 'react';

import axios from 'axios';

const useCitiesAPI = (cityName: string) => {
    const [cities, setCities] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [isError, setIsError] = useState(false);

    const url = `https://search.reservamos.mx/api/v2/places?q=${cityName}`;

    useEffect(() => {
        if (cityName === '') return;
        const fetchData = async () => {
            setIsError(false);
            setIsLoading(true);

            try {
                const result = await axios(url);
                setCities(result.data);
            } catch (error) {
                setIsError(true);
            }

            setIsLoading(false);
        };

        fetchData();
    }, [url]);

    return { cities, isLoading, isError };
};

export default useCitiesAPI;