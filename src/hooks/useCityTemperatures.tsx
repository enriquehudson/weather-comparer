import axios from 'axios';
import { Day } from '../common/types';

const useCityTemperatures = () => {
    const fetchTemperatures = async (lat: number, lon: number, apiKey: string) => {
        const url = `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&exclude=hourly,minutely,current&units=metric&appid=${apiKey}`;
        try {
            const response = await axios(url);
            const temperatures = response.data.daily;
            const result = temperatures.map((day: Day) => ({
                id: day.dt,
                max: day.temp.max,
                min: day.temp.min,
            }));
            return result;
        } catch (error) {
            console.error('Error', error);
        }
    };
    return fetchTemperatures;
};

export default useCityTemperatures;
