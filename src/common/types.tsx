export type SingleCity = {
    id: number;
    display: string;
    lat: number;
    long: number;
    temperatures: Temperature[];
};

export type Temperature = {
    id: number;
    max: number;
    min: number;
};

export type Day = {
    dt: any;
    temp: Temperature
}

export type TemperaturesProps = {
    temperatures: SingleCity[];
};

export interface SearchProps {
    addCity: (cityName: SingleCity) => void;
}
