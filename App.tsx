import { useState } from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Search from './src/components/Search';
import useCityTemperatures from './src/hooks/useCityTemperatures';
import Temperatures from './src/components/Temperatures';
import { SingleCity } from './src/common/types';
import SelectedCities from './src/components/SelectedCities';
import CaptureAPI from './src/components/CaptureAPI';

export default function App() {
  const [selectedCities, setSelectedCities] = useState<SingleCity[]>([]);
  const [apiToken, setApiToken] = useState<string>('');
  const fetchTemperatures = useCityTemperatures();

  const addCity = async (city: SingleCity) => {
    const temperatures = await fetchTemperatures(city.lat, city.long, apiToken);
    city.temperatures = temperatures;
    if (!selectedCities.some((selectedCity) => selectedCity.id === city.id)) {
      setSelectedCities([...selectedCities, city]);
    }
  }

  const removeCity = (id: number) => {
    setSelectedCities(selectedCities.filter((city) => city.id !== id));
  }

  if (!apiToken)
    return (
      <CaptureAPI setApiToken={setApiToken} />
    );

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Weather Comparer</Text>
      <Search addCity={addCity} />
      {selectedCities.length > 0 &&
        <SelectedCities selectedCities={selectedCities} removeCity={removeCity} />
      }
      <Temperatures temperatures={selectedCities} />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 40,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
    textAlign: 'center',
  },
});
